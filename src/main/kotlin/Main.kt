import com.soywiz.korim.bitmap.Bitmap32
import com.soywiz.korim.color.Colors
import com.soywiz.korim.color.RGBA
import com.soywiz.korim.format.PNG
import java.io.File
import java.lang.Integer.max

fun main() {
    val dangerousPoints = getDangerousPoints()
    println(dangerousPoints.size)
    val imageSize = getMaxXAndY(dangerousPoints)
    val bitmap = Bitmap32(imageSize.first, imageSize.second) { x, y -> Colors.WHITE }
    for (point in dangerousPoints) {
        bitmap[point.key.x, point.key.y] = RGBA.float(0, 0, 0, 1 - (1 / point.value.toFloat()))
    }
    File("out.png").writeBytes(PNG.encode(bitmap))
}

fun getDangerousPoints(): Map<Point, Int> {
    val allPoints = File("input.txt").readLines().map { getAllPointsOnStraight(it) }.flatten()
    val pointMap = hashMapOf<Point, Int>()
    allPoints.forEach {
        pointMap[it] = (pointMap[it] ?: 0) + 1
    }
    return pointMap.filter { it.value >= 2 }
}

fun getAllPointsOnStraight(string: String): List<Point> {
    val firstPoint = Point(string.split("->")[0])
    val lastPoint = Point(string.split("->")[1])
    val xValues = if (lastPoint.x > firstPoint.x) {
        firstPoint.x..lastPoint.x
    } else {
        firstPoint.x downTo lastPoint.x
    }.map { it }
    val yValues = if (lastPoint.y > firstPoint.y) {
        firstPoint.y..lastPoint.y
    } else {
        firstPoint.y downTo lastPoint.y
    }.map { it }
    val allPoints = mutableListOf<Point>()
    for (i in 0 until max(xValues.size, yValues.size)) {
        allPoints.add(Point(xValues.getOrNull(i) ?: firstPoint.x, yValues.getOrNull(i) ?: firstPoint.y))
    }
    return allPoints
}

fun getMaxXAndY(input: Map<Point, Int>): Pair<Int, Int> {
    var maxX = 0
    var maxY = 0
    input.forEach {
        if (it.key.x > maxX) {
            maxX = it.key.x
        }
        if (it.key.y > maxY) {
            maxY = it.key.y
        }
    }
    return Pair(maxX + 1, maxY + 1)
}

data class Point(val x: Int, val y: Int) {
    constructor(string: String) : this(string.trim().split(",")[0].toInt(), string.trim().split(",")[1].toInt())
}