import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MainKtTest {

    @Test
    fun getDangerousPointsTest() {
        assertEquals(21698, getDangerousPoints().size)
    }
}